--
-- Database: `demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `ne_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `comment_name` varchar(100) NOT NULL,
  `comment_email` varchar(100) NOT NULL,
  `comment_body` text NOT NULL,
  `comment_state` tinyint(1) NOT NULL DEFAULT '0',
  `comment_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`comment_id`, `ne_id`, `parent_id`, `comment_name`, `comment_email`, `comment_body`, `comment_state`, `comment_created`) VALUES
(1, 1, 0, 'first comment', 'firs@yahoo.com', 'first comment', 0, 1547180427),
(2, 1, 1, 'second comment', 'firs@yahoo.com', 'second comment', 0, 1547180427),
(3, 2, 2, 'third comment', 'firs@yahoo.com', 'third comment', 0, 1547180427),
(4, 2, 0, 'Fourth comment', 'firs@yahoo.com', 'Fourth comment', 0, 1547180427);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`comment_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
